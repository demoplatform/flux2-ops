#!/bin/bash

flux create image update team2-apps \
--git-repo-ref=team2-apps \
--git-repo-path="./dev" \
--checkout-branch=main \
--push-branch=main \
--author-name=fluxcdbot \
--author-email=fluxcdbot@users.noreply.github.com \
--commit-template="{{range .Updated.Images}}{{println .}}{{end}}" \
--export > ./clusters/eks-dev1/team2-apps-automation.yaml